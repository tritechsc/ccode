#include <iostream>
#include <string>
using namespace std;

int main()
{
	string hex_str= "1F1FA2";
	string red, green , blue;
	int number = 0;
	int r =0,g = 0,b = 0;

	number = stoi(hex_str, 0, 16);
	cout<<"hex_string: "<<hex_str<<endl;
	cout<<"number: "<<number<<endl;
	
	red = hex_str.substr(0,1);
	
	hex_str = "FFDDAA";
	number = stoi(hex_str, 0, 16);
	cout<<"hex_string: "<<hex_str<<endl;
	cout<<"number: "<<number<<endl;

	return 0;
}
